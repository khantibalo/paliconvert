var	sinhala_consonants=new Array(
		'\u0d9a', "k",
		'\u0d9b', "kh",
		'\u0d9c', "g",
		'\u0d9d', "gh",
		'\u0d9e', "ṅ",
		'\u0da0', "c",
		'\u0da1', "ch",
		'\u0da2', "j",
		'\u0da3', "jh",
		'\u0da4', "ñ",
		'\u0da7', "ṭ",
		'\u0da8', "ṭh",
		'\u0da9', "ḍ",
		'\u0DAA', "ḍh",
		'\u0dab', "ṇ",
		'\u0dad', "t",
		'\u0dae', "th",
		'\u0daf', "d",
		'\u0db0', "dh",
		'\u0db1', "n",
		'\u0db4', "p",
		'\u0db5', "ph",
		'\u0db6', "b",
		'\u0db7', "bh",
		'\u0db8', "m",
		'\u0dba', "y",
		'\u0dbb', "r",
		'\u0dbd', "l",
		'\u0dc0', "v",
		'\u0dc3', "s",
		'\u0dc4', "h",
		'\u0dc5', "ḷ");
	
var sinhala_vowels=new Array(
		//initial
		'\u0d85', "a",
		'\u0d86', "ā",
        '\u0d89', "i",
        '\u0d8a', "ī",
		'\u0d8b', "u",
		'\u0d8c', "ū",
        '\u0d91', "e",
        '\u0d92', "ē",
        '\u0d94', "o",
        '\u0d95', "ō",
        //diac
		'\u0dcf', "ā",
		'\u0dd2', "i",
		'\u0dd3', "ī",
		'\u0dd4', "u",
		'\u0dd6', "ū",
        '\u0dd9', "e",
        '\u0dda', "ē",
        '\u0ddc', "o",
        '\u0ddd', "ō"
			);

function sinhala_to_roman(sinhala)
{
	var output="";
	var roman_word="";
	var objPos={ position: 0 };
		
	while(objPos.position<sinhala.length)
	{	
		roman_word+=sinhala_letter_to_roman(sinhala.substr(objPos.position),objPos);
	}
	
	output+=" "+roman_word;

	
	return output;
}

function sinhala_letter_to_roman(word,objPos)
{
	var roman="";
	
	var first=word.substr(0,1);
	var second=word.substr(1,1);
	var third=word.substr(2,1);
	
	//initial vowels
	for(var i=0;i<sinhala_vowels.length;i+=2)
	{
		if(first==sinhala_vowels[i])
		{
			roman=sinhala_vowels[i+1];
			objPos.position+=1;
			//check hal kirima
			if(second=='\u0dca')
			{
				if(roman=="o")
					roman="ō";
				
				if(roman=="e")
					roman="ē";
				
				objPos.position+=1;
			}
			break;
		}
	}
			
	if(roman.length===0)
	{		
		//search consonants
		for(var i=0;i<sinhala_consonants.length;i+=2)
		{
			if(sinhala_consonants[i]==first)
			{
				roman=sinhala_consonants[i+1];
				break;
			}
		}				
		
		if(roman.length!=0)
		{
			var vowel_found=false;
			//search for vowels combined with consonants
			for(var i=0;i<sinhala_vowels.length;i+=2)
			{
				if(sinhala_vowels[i]==second)
				{
					roman+=sinhala_vowels[i+1];
					objPos.position+=2;
					
					//check for anusvara uṃ, iṃ

					if(third=='\u0D82')
					{
						roman+="ṃ";
						objPos.position++;
					}
					
					vowel_found=true;
					break;
				}
			}
							
			if(!vowel_found)
				switch(second)
				{
					case '\u0dca'://this consonant is without a vowel
						objPos.position+=2;
						break;
					case '\u0D82':
						roman+="aṃ";//anusvara
						objPos.position+=2;
						break;
					default:
						roman+="a";
						objPos.position++;
						break;
				}	
		}
		
		if(roman.length===0)
		{	//cannot convert
			roman=word.substr(0,1);
			objPos.position++;
		}
	}
	
	return roman.replace("\n", "<br>");
}