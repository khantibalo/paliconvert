﻿var consonants = new Array(
			'\u1000', "k",
			'\u1001', "kh",
			'\u1002', "g",
			'\u1003', "gh",
			'\u1004', "ṅ",
			'\u1005', "c",
			'\u1006', "ch",
			'\u1007', "j",
			'\u1008', "jh",
			'\u1009', "ñ",
            '\u100a', "ññ",
			'\u100b', "ṭ",
			'\u100c', "ṭh",
			'\u100d', "ḍ",
			'\u100e', "ḍh",
			'\u100f', "ṇ",
			'\u1010', "t",
			'\u1011', "th",
			'\u1012', "d",
			'\u1013', "dh",
			'\u1014', "n",
			'\u1015', "p",
			'\u1016', "ph",
			'\u1017', "b",
			'\u1018', "bh",
			'\u1019', "m",
			'\u101a', "y",
			'\u101b', "r",
			'\u101c', "l",
			'\u101d', "v",
            '\u1040', "v",
			'\u101e', "s",
			'\u101f', "h",
			'\u1020', "ḷ",
            '\u103f', "ss");

var vowels = new Array(
		'\u1021', "a",
        '\u1023', "i",
        '\u1024', "ī",
		'\u1025', "u",
		'\u1026', "ū",
        '\u1027', "e",
        '\u1029', "o",
		'\u102b', "ā",//после согласной. участвует в образовании о
        '\u102c', "ā",//после согласной. участвует в образовании о
		'\u102d', "i",
		'\u102e', "ī",
		'\u102f', "u",
		'\u1030', "ū",
        '\u103e', "h");

//\u1031 e - перед согласной
//\u1036 - анусвара
//\u1039 - вирам

var spec = new Array(
		'\u104b', ".",
        '\u104a', ",",
		'\u1040', "0",
		'\u1041', "1",
		'\u1042', "2",
		'\u1043', "3",
		'\u1044', "4",
		'\u1045', "5",
		'\u1046', "6",
		'\u1047', "7",
		'\u1048', "8",
		'\u1049', "9"
);

function burmese_to_roman(burmese) {
    var output = "";
    var roman_word = "";
    var objPos = { position: 0 };

    while (objPos.position < burmese.length) {
        roman_word += letter_to_roman(burmese.substr(objPos.position), objPos);
    }

    output += " " + roman_word;


    return output;
}

function letter_to_roman(word, objPos) {
    var roman = "";

    var first = word.substr(0, 1);
    var second = word.substr(1, 1);
    var third = word.substr(2, 1);

    //initial vowels
    for (var i = 0; i < vowels.length; i += 2) {
        if (first == vowels[i]) {
            if (second == '\u102c') {
                roman = "ā";
                objPos.position+=2;
            }
            else if (second == '\u1039') {
                roman = 'ñ';
                objPos.position += 2;
                break;
            }
            else {
                roman = vowels[i + 1];
                objPos.position++;
            }
            break;
        }
    }

    if (roman.length === 0) {
        //special handling for o and e
        var setO = false;
        var setE = false;

        if (first == '\u1031') {
            if(third=='\u102b' || third=='\u102c')
            {
                first = second;
                second = third;
                third = word.substr(3, 1);
                setO = true;
            }
            else
            {
                first = second;
                second = third;
                third = word.substr(3, 1);
                setE = true;
            }
        }

        //search consonants
        for (var i = 0; i < consonants.length; i += 2) {
            if (consonants[i] == first) {
                roman = consonants[i + 1];
                break;
            }
        }

        if (roman.length !== 0) {
            if (setO == true || setE == true) {

                if (setO == true) {
                    roman += "o";
                    objPos.position += 3;
                }

                if (setE == true) {
                    roman += "e";
                    objPos.position += 2;
                }
            }
            else {
                var vowel_found = false;

                if (third == " ") {
                    var fourth = word.substr(3, 1);

                    if (fourth == '\u1071') {
                        roman += "t";
                        objPos.position += 2;
                    }

                    if (fourth == '\u107b') {
                        roman += "b";
                        objPos.position += 2;
                    }
                }


                //search for vowels combined with consonants
                for (var i = 0; i < vowels.length; i += 2) {
                    if (vowels[i] == second) {
                        roman += vowels[i + 1];
                        objPos.position += 2;

                        //check for anusvara uṃ, iṃ

                        if (third == '\u1036') {
                            roman += "ṃ";
                            objPos.position++;
                        }

                        vowel_found = true;
                        break;
                    }
                }

                if (!vowel_found)
                    switch (second) {
                    case '\u1031': //e или o
                        if (third == '\u102b' || third == '\u102c') {
                            roman += "o";
                            objPos.position += 3;
                        }
                        else {
                            roman += "e"; //next consonant will be with O or E
                            objPos.position += 2;
                        }
                        break;
                    case '\u1039': //this consonant is without a vowel
                        objPos.position += 2;
                        break;
                    case '\u1036':
                        roman += "aṃ"; //anusvara
                        objPos.position += 2;
                        break;
                    case '\u103c':
                        roman += "r";
                        objPos.position += 2;
                        break;
                    case '\u103d':
                        roman += "v";
                        objPos.position += 2;
                        break;
                    case ' ':
                        if (third == '\u1071') {
                            roman += "ta";
                            objPos.position += 3;
                        }
                        else {
                            roman += "a";
                            objPos.position++;
                        }
                        break;
                    case '\u103a':
                        if (third == '\u1039')
                            objPos.position += 3;
                        else {
                            roman += "taṃ";
                            objPos.position += 2;
                        }

                        break;
                    default:
                        roman += "a";
                        objPos.position++;
                        break;
                }


            }
        }

        if (roman.length === 0) {//spec char
            for (var i = 0; i < spec.length; i += 2) {
                if (first == spec[i]) {
                    roman = spec[i + 1];

                    if (second == '\u1040') {
                        roman += "0";
                        objPos.position += 2;
                    }
                    else
                        objPos.position++;

                    break;
                }
            }
        }

        if (roman.length === 0) {	//cannot convert
            roman = word.substr(0, 1);
            objPos.position++;
        }
    }

    return roman.replace("\n", "<br>");
}