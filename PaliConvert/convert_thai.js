var	thai_consonants=new Array(
			"ก","k",
			"ข","kh" ,
			"ค","g",
			"ฆ","gh" ,
			"ง","ṅ",
			"จ","c",
			"ฉ","ch",
			"ช","j",
			"ฌ","jh",
			"ญ","ñ",
			"ฏ","ṭ",
			"ฐ","ṭh",
			"ฑ","ḍ",
			"ฒ","ḍh",
			"ณ","ṇ",
			"ต","t",
			"ถ","th",
			"ท","d",
			"ธ","dh",
			"น","n",
			"ป","p",
			"ผ","ph",
			"พ","b",
			"ภ","bh",
			"ม","m",
			"ย","y",
			"ร","r",
			"ล","l",
			"ว","v",
			"ส","s",
			"ห","h",
			'\uF70F',"ñ",
			"ฬ","ḷ",
			'\uF700',"ṭh");
	
var thai_vowels=new Array(
		"า","ā",
		'\u0E38',"u",
		'\u0E39',"ū",
		'\u0E34',"i",
		'\u0E35',"ī",
		'\u0E36',"iṃ",
		'\uF701',"i",
			"อา","ā",
			"อิ","i",
			"อี","ī",
			"อุ","u",
			"อู","ū",
			"เอ","e",
			"โอ","o"
			);

var thai_spec=new Array(
		"ฯ",".",
		"๐","0",
		"๑","1",
		"๒","2",
		"๓","3",
		"๔","4",
		"๕","5",
		"๖","6",
		"๗","7",
		"๘","8",
		"๙","9"		
);

function thai_to_roman(thai)
{
	var output="";
	var roman_word="";
	var objPos={ position: 0 };
		
	while(objPos.position<thai.length)
	{	
		roman_word+=thai_letter_to_roman(thai.substr(objPos.position),objPos);
	}
	
	output+=" "+roman_word;

	
	return output;
}

function thai_letter_to_roman(word,objPos)
{
	var roman="";
	
	var first=word.substr(0,1);
	var second=word.substr(1,1);
	var pair=word.substr(0,2);
	var third=word.substr(2,1);
	
	//initial vowels
	for(var i=0;i<thai_vowels.length;i+=2)
	{
		if(pair==thai_vowels[i])
		{
			roman=thai_vowels[i+1];
			objPos.position+=2;
			break;
		}
	}
		
	if(roman.length===0 && first=="อ")
	{
		roman="a";
		objPos.position++;		
	}
	
	if(roman.length===0)
	{
		//special handling for o and e
		var setO=false;
		var setE=false;
		
		if(first=="โ")
		{
			first=second;
			second=third;
			third=word.substr(3,1);
			setO=true;
		}
		
		if(first=="เ")
		{
			first=second;
			second=third;
			third=word.substr(3,1);
			setE=true;
		}
		
		//search consonants
		for(var i=0;i<thai_consonants.length;i+=2)
		{
			if(thai_consonants[i]==first)
			{
				roman=thai_consonants[i+1];
				break;
			}
		}				
		
		if(roman.length!==0)
		{
			if(setO==true || setE==true)
			{
				//special case for khve and khvo
				if(first==thai_consonants[2] && (second=='\uF71A' || second=='\u0E3A') && 
						third==thai_consonants[56])
				{
					roman="khv";
					objPos.position+=2;
				}
				
				if(setO==true)
				{
					roman+="o";
					objPos.position+=2;
				}
				
				if(setE==true)
				{
					roman+="e";
					objPos.position+=2;
				}
			}
			else
			{
				var vowel_found=false;
				//search for vowels combined with consonants
				for(var i=0;i<thai_vowels.length;i+=2)
				{
					if(thai_vowels[i]==second)
					{
						roman+=thai_vowels[i+1];
						objPos.position+=2;
						
						//check for anusvara uṃ, iṃ
	
						if(third=='\uF711' || third=='\u0E4D')
						{
							roman+="ṃ";
							objPos.position++;
						}
						
						vowel_found=true;
						break;
					}
				}
				
				if(!vowel_found && second==" " && third=='\u0E3A')
				{
					objPos.position+=3;
					vowel_found=true;
				}
				
				if(!vowel_found)
					switch(second)
					{
						case "เ":
						case "โ":
							roman+="a";//next consonant will be with O or E
							objPos.position++;
							break;
						case '\uF71A':
						case '\u0E3A'://this consonant is without a vowel
							objPos.position+=2;
							break;
						case '\uF711':
						case '\u0E4D':
							roman+="aṃ";//anusvara
							objPos.position+=2;
							break;
						default:
							roman+="a";
							objPos.position++;
							break;
					}	
				
	
			}
		}
		
		if(roman.length===0)
		{//spec char
			for(var i=0;i<thai_spec.length;i+=2)
			{
				if(first==thai_spec[i])
				{
					roman=thai_spec[i+1];
					objPos.position++;
					break;
				}
			}
		}
		
		if(roman.length===0)
		{	//cannot convert
			roman=word.substr(0,1);
			objPos.position++;
		}
	}
	
	return roman.replace("\n", "<br>");
}